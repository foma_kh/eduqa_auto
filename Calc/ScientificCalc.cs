﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calc
{
    public class ScientificCalc : SimpleCalc
    {
        public double Power(double a, int pow)
        {
            double res = a;
            if (pow == 0)
            {
                res = 1;
            }
            else
            {
                for (int i = 1; i < pow; i++)
                {
                    res = Mult(res, a);
                }
            }
            return res;
        }

        public double Percents(double a, double b)
        {
            double res = Mult(Div(a, b), 100);
            return res;
        }

        public double ModDiv(double a, double b)
        {
            if (a > 0 && b > 0)
            {
                while (a >= b)
                {
                    a = Sub(a, b);
                }
            }
            else if (a < 0 && b < 0)
            {
                while (a <= b)
                {
                    a = Sub(a, b);
                }
            }
            else if (a < 0 && b > 0)
            {
                while (a <= -b)
                {
                    a = Sum(a, b);
                }
            }
            else if (a == 0 && b != 0)
            {
                a = 0;
            }
            else if (b == 0)
            {
                Console.WriteLine("Error, Divide by 0");
            }
            return a;
        }

        public double ArraySum(double[] array)
        {
            double res = 0;
            for (int i = 0; i < array.Length; i++)
            {
                res += array[i];
            }
            return res;
        }

        public double ArrayMin(double[] ar)
        {
            double min = ar[0];
            for (int i = 1; i < ar.Length; i++)
            {
                if (ar[i] < min)
                {
                    min = ar[i];
                }
            }
            return min;
        }

        public double ArrayMax(double[] ar)
        {
            double max = ar[0];
            for (int i = 1; i < ar.Length; i++)
            {
                if (ar[i] > max)
                {
                    max = ar[i];
                }
            }
            return max;
        }
    }
}
