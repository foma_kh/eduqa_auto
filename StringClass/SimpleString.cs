﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringClass
{
    public class SimpleString
    {
        public String MergeString(String str1, String str2)
        {
            return str1 + str2;
        }

        public String ReverseString(String str)
        {
            String res = "";
            int len = str.Length - 1;
            for (int i = len; i >= 0; i--)
            {
                res += str.Substring(i, 1);
            }
            return res;
        }
    }
}
