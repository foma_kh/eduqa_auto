﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Calc;

namespace TestCalc
{
    [TestFixture]

    public class ScientificCalcTest
    {
        ScientificCalc scientificCalc = new ScientificCalc();
        double[] ar = { 2.0, 5.1, 3.0, -5.1 };

        [Test, Order(1)]
        public void TestPowInt()
        {
            Assert.AreEqual(125, scientificCalc.Power(5, 3), "5^3 should be equal to 125");
        }

        [Test, Order(2)]
        public void TestPowDouble()
        {
            Assert.AreEqual(3802.04, scientificCalc.Power(5.2, 5), 0.01, "5.2^5 should be equal to 3802.04");
        }

        [Test]
        public void TestPowNeg()
        {
            Assert.AreEqual(5, scientificCalc.Power(5, -2), "Can`t use negative value for pow");
        }

        [Test]
        public void TestPercents()
        {
            Assert.AreEqual(50, scientificCalc.Percents(50, 100), 0.01, "50/100 should be equal to 50%");
        }

        [Test]
        public void TestModDiv()
        {
            Assert.AreEqual(2, scientificCalc.ModDiv(5, 3), "5%3 should be equal to 2");
        }

        [Test]
        public void TestModDivEqual()
        {
            Assert.AreEqual(0, scientificCalc.ModDiv(5, 5), "5%5 should be equal to 0");
        }

        [Test]
        public void TestModDivNegTwoValue()
        {
            Assert.AreEqual(-1, scientificCalc.ModDiv(-9, -4), "-9%-4 should be equal to -1");
        }

        [Test]
        public void TestModDivNegFirstValue()
        {
            Assert.AreEqual(-1, scientificCalc.ModDiv(-9, 4), "-9 % 4 should be equal to -1");
        }

        [Test]
        public void TestModDivNegFirstValue2()
        {
            Assert.AreEqual(-12, scientificCalc.ModDiv(-78, 33), "-9 % 4 should be equal to -1");
        }

        [Test]
        public void TestModDivZeroFirstValue()
        {
            Assert.AreEqual(0, scientificCalc.ModDiv(0, 33), "0 % 33 should be equal to 0");
        }

        [Test]
        public void TestArraySum()
        {
            Assert.AreEqual(5.0, scientificCalc.ArraySum(ar), "Sum { 2.0, 5.1, 3.0, -5.1 } should be equal to 5.0");
        }

        [Test]
        public void TestArrayMax()
        {
            Assert.AreEqual(5.1, scientificCalc.ArrayMax(ar), 0.1, "Sum { 2.0, 5.1, 3.0, -5.1 } should be equal to 5.1");
        }

        [TestCase(10, 2, 100)]
        [TestCase(9, 3, 100)]
        [TestCase(5, 3, 100)]
        [TestCase(100, 1, 100)]
        public void TestPowerGreater(double a, int b, double c)
        {
            Assert.GreaterOrEqual(scientificCalc.Power(a, b), c);
        }

        [Test]
        public void TestArrayMin()
        {
            Assert.AreEqual(-5.1, scientificCalc.ArrayMin(ar), 0.01, "Min { 2.0, 5.1, 3.0, -5.1 } should be equal to 2");
        }
    }
}




