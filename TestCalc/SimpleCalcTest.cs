﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Calc;

namespace TestCalc
{
    [TestFixture]

    public class SimpleCalcTest
    {
        SimpleCalc simpleCalc = new SimpleCalc();

        [Test, Order(1)]
        public void TestSumDouble()
        {
            Assert.AreEqual(5.7, simpleCalc.Sum(3.2, 2.5), "3.2 + 2. 5 should be equal to 5.7");
        }

        [Test]
        [Ignore("Skip this test")]
        public void TestSumFail()
        {
            Assert.AreNotEqual(1, simpleCalc.Sum(3, -2), "3 + -2 should be equal to 1");
        }

        [Test, Order(2)]
        public void TestSubDouble()
        {
            Assert.AreEqual(-6.3, simpleCalc.Sub(-3.7, 2.6), 0.01, "-3.7 -2.6 should be equal to -6.3");
        }

        [Test]
        public void TestSubInt()
        {
            Assert.AreEqual(5, simpleCalc.Sub(3, -2), "3 - (-2) should be equal to 5");
        }

        [Test]
        [Retry(3)]
        public void TestMultIntFail()
        {
            Assert.AreEqual(4200, simpleCalc.Mult(60, -70), "60*70 should be equal to 4200");
        }

        [Test]
        public void TestMultDouble()
        {
            Assert.AreEqual(-1.15, simpleCalc.Mult(0.5, -2.3), 0.01, "0.5*(-2.3) should be equal to -1.15");
        }

        [Test, Order(3)]
        public void TestDivInt()
        {
            Assert.AreEqual(33.33, simpleCalc.Div(100, 3), 0.01, "100/3 should be equal to 33.33");
        }

        [Test]
        [Repeat(2)]
        public void TestDivDouble()
        {
            Assert.AreEqual(11.68, simpleCalc.Div(11.1, 0.95), 0.01, "11.1/0.95 should be equal to 11.68");
        }

        [Test]
        public void TestDivZero()
        {
            {
                Assert.Throws<DivideByZeroException>(new TestDelegate(() => simpleCalc.Div(5, 0)));
            }
        }

        [TestCase(2, 2, 4)]
        [TestCase(6, 6, 12)]
        [TestCase(1, -3, -2)]
        [TestCase(2.5, 100, 102.5)]
        public void TestAdd(double a, double b, double c)
        {
            Assert.AreEqual(c, simpleCalc.Sum(a, b));
        }

        [TestCase(20, 5, ExpectedResult = 4)]
        [TestCase(16, 2, ExpectedResult = 8)]
        public double DivTest(double n, double d) => simpleCalc.Div(n, d);
    }
}
