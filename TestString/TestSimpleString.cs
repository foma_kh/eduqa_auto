﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using StringClass;

namespace TestString
{
    [TestFixture]
    public class TestSimpleString
    {
        SimpleString simpleStringTest = new SimpleString();

        [TestCase("miami", ExpectedResult = "imaim")]
        [TestCase("visual", ExpectedResult = "lausiv")]
        [TestCase("автоматизация", ExpectedResult = "яицазитамотва")]
        public String TestReverseSimpleString(String s1)
        {
            return simpleStringTest.ReverseString(s1);
        }

        [Test]
        public void TestMergeString()
        {
            StringAssert.AreEqualIgnoringCase("selectall", simpleStringTest.MergeString("Select", "All"));
        }

        [Test]
        public void TestMergeReverseStringContains()
        {
            StringAssert.Contains("cel", simpleStringTest.ReverseString(simpleStringTest.MergeString("Select", "All")));
        }

        [Test]
        public void TestMergeReverseStringStart()
        {
            StringAssert.StartsWith("ll", simpleStringTest.ReverseString(simpleStringTest.MergeString("Select", "All")));
        }

        [Test]
        public void TestMergeReverseStringEnd()
        {
            StringAssert.EndsWith("eS", simpleStringTest.ReverseString(simpleStringTest.MergeString("Select", "All")));
        }

        [Test]
        public void AlwaysPass()
        {
            Assert.Pass("Always pass test");
            StringAssert.EndsWith("no", simpleStringTest.ReverseString(simpleStringTest.MergeString("Select", "All")));
        }

        [Test]
        public void AlwaysFail()
        {
            StringAssert.EndsWith("eS", simpleStringTest.ReverseString(simpleStringTest.MergeString("Select", "All")));
            Assert.Fail("Always pass test");
        }
    }
}
