﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Calc;


namespace TestCalc
{
    [TestFixture]
    public class ScientificCalcSetupTest
    {
        ScientificCalc scientificSet = new ScientificCalc();
        double[] ar = { 2, 3, 5, 1 };
        int[] iArray = new int[] { 10, 20, 50 };

        [OneTimeSetUp]
        public void RunBeforeTests()
        {
            iArray[0] = 51;
        }

        [OneTimeTearDown]
        public void RunAfterTests()
        {
            iArray[0] = 10;
        }
                
        [SetUp]
        public void Init()
        {
            for (int i = 0; i < ar.Length; i++)
                ar[i] = ar[i] * 2;
        }

        [TearDown]
        public void CleanUp()
        {
            for (int i = 0; i < ar.Length; i++)
                ar[i] = ar[i] / 2;
        }

        [Test]
        public void TestSetUpArrayMin()
        {
            Assert.AreEqual(2, scientificSet.ArrayMin(ar), "Min { 4.0, 6.0, 10.0, 2.0 } should be equal to 2");
        }

        [Test]
        public void TestSetUpArrayMax()
        {
            Assert.That(scientificSet.ArrayMax(ar), Is.GreaterThan(scientificSet.ArrayMin(ar)).And.LessThan(10.01));
        }

        [Test,Description("This test shows the work of OneTimeTearDown")]
        public void TestIArrayIsInRange()
        {
            Assert.That(iArray, Is.All.InRange(0, 50));
        }

        [Test]
        public void TestArrayMinIsGreaterThanZero()
        {
            Assert.That(scientificSet.ArrayMin(ar) >= 0, Is.True);
        }
    }
}
